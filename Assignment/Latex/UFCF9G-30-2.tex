\documentclass[a4paper,11pt,oneside]{article}
\input{standardheader.tex}
\begin{document}
\parindent=0pt
\parskip=.4cm

\begin{center}
{\LARGE Mobile and Physical Computing (UFCF9G-30-2) - Assignment 1 \\}
\end{center}

\section{Important Info and Dates}
\begin{itemize}
\item The submission deadline for the assignment is \textbf{Thursday 6th December 2018 at  2pm} (submitted via Blackboard). 
\item Your result for Assignment 1 will make up 25\% of the module mark. 
\item You are required to work in groups of 2 for this assignment.
\item At some point before \textbf{Thursday 25th October 2018}, a member of your group must email\\ tom.mitchell@uwe.ac.uk with your names ensuring that the other group member is cc'd. \textbf{If you are not allocated to a group you will be unable to submit your work}.
\item One electronics kit will be available for each group and must be collected by both group members. 
\item The kit must be returned by \textbf{Friday 14th December 2018}.
\end{itemize}

\section{The Assignment}
This assignment provides you with an opportunity to demonstrate your ability to apply the theory and methods taught in the lectures and practical sessions for this module. It also provides space to build a music or audio interface on a greater scale and depth than has been possible in the practical sessions. 

Your group is required to design, create, and document a physical interface to control a musical or audio process. Your work must focus on the interaction design, electronics, signal conditioning and mapping decisions that go into the development of the interface. 

The interface must be created using the electronics parts in the provided kit. You may use these parts on their own or attach them to other physical objects to form part of your interface. All user interactions (including any feedback) should take place via your interface which \textbf{must not} require the user to observe/use standard computer interfaces (QWERTY keyboard, mouse, monitor, etc.). 

The focus here is about making appropriate interface and interaction design decisions to alter or generate sound. Details of the audio processing are \textbf{not} the focus for this assignment. Consequently you are free to use existing audio plugins or example MaxMSP audio processes found elsewhere (credited appropriately), you will not be assessed on your ability to create the audio processing component of the system from scratch.

\subsection{Assignment Components}
Your group must:
\begin{enumerate}
\item design and construct the physical interface and associated electronics
\item design and program the signal conditioning and mappings that convert user interactions into audio process controls
\item write a report documenting the design, development and evaluation of the interface
\item produce a short one-take video demonstrating the interface
\end{enumerate}

\subsection{Assignment Deliverables}
These deliverables should be submitted via blackboard (one submission per group). The submission should clearly indicate each group members' student number and include the following sections.  

\subsubsection*{Written Report (70 Marks)}
The report of 1000 words ($\pm$10\%), submitted as a single coherent document written in the third person including cover/contents pages, section/page numbers, images (photographs, block/circuit diagrams and illustrations) with figure numbers and references (in the UWE Harvard style \url{www.uwe.ac.uk/referencing/uweharvard}). The document should be in a common file format such as \texttt{.pdf} or \texttt{.doc} and structured as follows:

\begin{enumerate}
\item \textbf{Abstract} This section will briefly summarise the work and provide relevant background information.
\item \textbf{Interaction Design} This section should describe your interface and motivation for your choice before expanding into the following subsections:
\begin{enumerate}
\item \textbf{Interface Design and Mappings} This subsection will clearly set out the design of the interface. It will describe your choice of mappings that link physical interactions with audio controls. It will include clear rationale for your mapping choices, referencing and incorporating interaction design ideas and concepts introduced throughout the module so far.
\item \textbf{Feedback} This subsection will provide an overview of the feedback mechanisms that have been created (via the vibration motors or LEDs) to support the performer. Again, include clear rational behind your choices here.
\end{enumerate}
\item \textbf{Implementation} This section will describe the implementation, including a description (with supporting diagrams) indicating the major system components (e.g. physical interface $ \rightarrow$ MaxMSP ($\rightarrow$ Logic Pro)) indicating their interconnecting communications protocols (OSC, MIDI), etc. The implementation description should then be expanded into the following sections:
\begin{enumerate}
\item \textbf{Electronics} This subsection should describe your chosen components (sensors, LEDs, vibration motors, etc). It will include clear rationale describing how these choices are appropriate for detecting the chosen interactions, providing the intended user feedback. This should correlate with details outlined in the interaction design section. This subsection should include a full circuit diagram showing all sensor connections, plus photographs of the interface with the electronic components (sufficient for the assessor to remake the instrument from scratch if necessary).
\item \textbf{Signal Conditioning} This subsection should communicate the signal conditioning and processing methods that were required to convert the raw sensor readings into music/audio controlling parameters. It will include clear rationale for your choices.
\end{enumerate}
\item \textbf{Conclusions} This section will form a reflective commentary on the completed work. You may wish to discuss any of the following:
\begin{enumerate}
\item An evaluation of the device commenting on its strengths and weaknesses
\item Your achievements and learning experiences
\item A summary of notable problems and solutions
\item A discussion of your time and group management
\item Any other notable comments relating to your experiences
\end{enumerate}
\end{enumerate}

\subsubsection*{Video Demonstration (10 Marks)}
The video should provide a demonstration/performance of the developed interface to be enjoyed by a broad audience, it should be informative and attract attention via social media networks. The footage should be approximately 1 minute in duration, clearly communicating and demonstrating the interface and how it is used. There should be limited technical information included, focus should be placed on the physical interactions and the resulting audio response with a performance and demonstration accompanying/interlaced with a verbal description. 

The video should be submitted in a common video format that can be played using a standard video player on the workstations in the lab (.mp4, .mov, .avi, etc.) and should be no more than 20 Mb. You are free to direct the video as you please but please note that beyond being able to see and hear the footage clearly there are no marks available for the aesthetic quality or editing. For example do not spend lots of time including titles, dubbed narration, special lighting, or editing as there are no marks available for these aspects. The video must be recorded in a \textbf{single take}. 

\subsubsection*{Implementation (20 Marks)}
All MaxMSP patches and other supporting arrangement files (Logic Pro, etc.) should be uploaded with your submission (if there are multiple supporting files they should be compressed into a single .zip file that should not exceed 10 Mb - make sure that the zip file can be expanded without error before submission). The assessors will be focussing on the interface that you develop (design, electronics, signal conditioning, mappings, etc.), therefore it is acceptable that the controlled audio processes (e.g. Max patches, plugins, etc) may originate from elsewhere (although the original developers must be credited accordingly). You must ensure that the assessor is able to run the entire system on a UWE workstation in 2N26/48 using the files that you submit.

\section{Group work}
You are required to work in pairs. A group representative must email \textbf{tom.mitchell@uwe.ac.uk} with the names of your group members before a kit can be collected. If you do not email by October 27th you will be paired with another group member at random and notified by email on Thursday 2nd November, \textbf{group changes after this date will not be permitted}. 

\section{Electronics Kits}
Your group will be allocated an electronics kit which must be collected by both group members. One group member will be responsible for the kit and ensuring it is returned complete and in working order by \textbf{Friday 14th December 2018}. These group members will be responsible for replacing any damaged or missing equipment. Loaning of equipment between groups is encouraged, but you must ensure that a complete kit is returned. 

\section{Online Submission}
Your group must submit all files online via the `Assignments' section on blackboard. You must allow sufficient time to cater for any unforeseen internet connection issues that may occur when attempting to upload files to ensure that all everything is submitted \textbf{before} the deadline. 

\section{Assignment Ideas and Examples}
Unique ideas beyond those listed here are highly encouraged.\\
\begin{enumerate}
\item \textbf{Augmented Instrument} Where an existing acoustic/electric instrument is thoughtfully enhanced. Projects like this must consider the `spare bandwidth' that is available with the chosen instrument\\
\item	\textbf{Music Object} Where a non musical object is transformed into a music/audio interface/instrument. The chosen audio process should be relevant to the object, and the electronics, signal conditioning and mappings should involve sensing interactions that are typically performed with this object. Some examples:\\
\begin{itemize}
%\item \textbf{Musical TV Remote} Where movements and button presses with a TV remote are transformed into music.
%\item \textbf{Ironing Instrument} Where ironing gestures, temperature adjustments, button presses, distance to the ironing surface contribute to the generation of music.
%\item \textbf{Scooter Music} Where movements while riding a scooter, proximity to pedestrians, buttons and knobs on the handle bars contribute to the generated sound.
\item \textbf{Lamp Music} Where manipulations of an angle poise lamp, rotations and button presses, etc. produce music.
\item \textbf{Christmas Bauble Music} Where a number of christmas baubles are hung from a frame their movements are sensed and used to synthesise music (a la, Reich's pendulum music)
\item \textbf{Music Glove} where multiple flex sensors are borrowed from other groups to make a glove, with pitch/roll and drum control.
\item \textbf{Musical Tennis Racket} where buttons, distance sensors and accelerometers are used to turn the racket into a guitar.
\end{itemize}
\item	\textbf{DJ Interface} An extension of the GyroScratch exercise (practical06) in which 2 (or even 3) x-OSCs are used (one borrowed temporarily from another group), one device acts as a record and the other as the mixer, to act as kill switches, crossfader and eq controls.
\item \textbf{Music Performance Interface} A novel or development of an existing interface that considers alternative approaches to music performance. 
\end{enumerate}

\section{Assessment/Grading Criteria}
It should be possible to complete the assignment to the minimum standard of 40\% by combining and extending aspects of the practical exercises into a working application. The work \textbf{must} extend the exercises completed in the practical sessions sufficiently to achieve a pass mark. 
\begin{itemize}
\item Marks above 40\% are available for the development of imaginative interfaces that capture physical interactions using inventive application of the provided sensing technology and will be awarded based upon the depth of subject knowledge evedent in the submitted material. 
\item Marks in the range 40-49\% will be awarded for an extension/synthesis of the more involved practical exercises through combination (with other practical ideas) and/or multiplication (extending practical ideas usefully). 
\item Marks in the range 50-59\% will be awarded for work that shows a clear understanding of the electronics, signal conditioning and mapping material, taught in practical sessions by developing an interface that incorporates the HCI ideas and theory covered in the lectures and reading. 
\item Marks in the range 60-69\% will be awarded for work that shows notably creative application of the taught theoretical and practical elements of the module. 
\item Marks in excess of 70\% will be awarded for work that shows significant flair above and beyond expectation. 
\end{itemize}

The marks for the assignment will be allocated as follows:
\subsection*{Report (70 marks)}
\begin{tabular}{|l | p{14cm} | }\hline
49 - 70	& The complete and well presented report documents an exceptional application of module material with a carefully selected interface that combines a range of well considered physical interactions, control mappings, feedback relevant to the controlled audio/musical processes that applies the main theoretical work introduced in lectures and practicals. The implementation section outlines a clear and refined application of the sensing and actuating technology implementing the most appropriate signal conditioning methods.\\\hline
42 - 48.9 	& The report outlines a well considered application of the module material in the development of an interface that combines the sensing of application specific physical actions mapped carefully to appropriate audio/music control processes with thoughtful visual and sonic feedback. The work shows some consideration of the related theoretical work although further detail could have been considered. The implementation section demonstrates a very competent application of sensing and actuating technology with well developed and appropriate signal conditioning and mapping methods.\\\hline
35 - 41.9	& The report documents the applied elements of module material, but the choice of interface and/or application/processing of sensors limits the marks available. The audio/music processes are not controlled by thoughtful interactions that one might expect with the choice of interface. Significant elements of the theoretical work relating to the design of digital musical instruments may have been overlooked. The report may include presentation and formatting issues and some sections may lack sufficient depth.\\\hline
28 - 34.9	& The report documents a very limited choice of interface where the application/processing of sensors betrays a shallow appreciation of the material covered throughout the module. The mapping of physical interactions to generated sound is not clearly justified, arbitrary, and/or poorly documented. The report may have missing sections and/or lack depth.\\\hline
0 - 27.9	& Many sections may be missing, in particular, the discussion of actions, mapping and sound choice may have been overlooked. Included sections do not include more than a partial application of the material covered in practicals. \\\hline
\end{tabular}

\subsection*{Video (10 marks)}
\begin{tabular}{| l | p{14cm} | }\hline
7 - 10 	& An exceptionally creative, concise and comprehensive description and performance/demonstration of the object with its supported physical interactions and music/audio response are imaginatively and clearly communicated. The content demonstrates a well planned narrative and rehearsed script without obvious errors or hesitations. \\\hline
6 - 6.9 	& A concise and comprehensive description and performance/demonstration of the object. The range of supported physical actions and musical response are clearly communicated. Presentation is well planned and delivered suggesting that the recording was the best of multiple takes. \\\hline
5 - 5.9	& The instrument and its interactive elements are clearly visible and audible. The object is demonstrated, although some of its features may not be entirely clear. The footage includes some unplanned or unprofessional elements and may have benefited from more imaginative planning and/or rehearsing. \\\hline
4 - 4.9	& The important elements of the instrument are not clear from the video. Supported physical actions or musical responses of the instrument may not have been demonstrated fully or communicated clearly, or the instrument is so limited that high marks may not awarded. Elements of the footage could have been expanded further and may have benefited from further rehearsing, narration and/or demonstration.\\\hline
0 - 3.9	&  It is not possible to decipher the features of instrument from the footage or the instrument appears non-functional. It may not have been possible to view the media.\\\hline
\end{tabular}

\subsection*{Implementation (20 marks)}
\begin{tabular}{|l | p{14cm} |}\hline
14 - 20	& The designed electronics, designed software and mappings to the audio/music processes represent outstanding implementation of the ambitious aims of the assignment outlined in the report with neat presentation of electronic circuits (in the report) and software including abstractions/subpatchers and commenting. The chosen sensing technology and signal conditioning are also near ideal for the interactions that they are designed to identify. \\ \hline
12 - 13.9 	& The electronics, signal conditioning and mapping processes are well designed and largely functional although certain elements of the electronics and/or software could have been improved.\\ \hline
10 - 11.9	& The electronics, signal conditioning and mapping may include minor bugs/glitches but is functional. The implementation quite simple and higher marks could not be awarded.\\ \hline
8 - 9.9		& The electronics, signal conditioning and mapping may include errors and may not align with the processes described in the report. The result maybe uncontrolled or unstable output. The implementation for the interface is so simple that higher marks could not be awarded. \\ \hline
0 - 7.9		& The electronics, signal conditioning and mapping do not demonstrate a working attempt at the assignment brief. The implementation for the interface does not extend the practical work in any way.\\ \hline
\end{tabular}

\end{document}


