\rule[.4cm]{\textwidth}{2pt}
\section{Practical 4 - Signal Conditioning and Switches}
\label{sec:prac04}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this practical we will explore further signal conditioning methods and learn how to connect digital switches to x-OSC. By the end of this practical you should understand:

\begin{enumerate}
\item how to use hysteresis to enable clean switching of an analogue signal.
\item how to connect and use switches.
\end{enumerate}

%- Hysteresis Flick the flex sensor (then IR sensor) to play a string sound. KS Algorithm.
%- Debouncing - Rate limiting.
%- Digital inputs - game controller thing.
%- Statemachines - switches toggle other sensors. Press the buttons to map the IR sensors to the three synth parameters.
%- One to many mappings.

%- String add keyboard.
%-

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Getting Started}
\begin{enumerate}

\item Find your \texttt{\textbf{/MobileAndPhysicalComputing}} folder on your Network storage space.

\item Now navigate to the folder \\\texttt{Desktop/StudentShared/fet/CSCT/MobileAndPhysicalComputing-UFCF9G-30-2/} and copy the entire folder \texttt{\textbf{Practical04}} to your  \texttt{\textbf{/MobileAndPhysicalComputing}} folder.

\item Launch MaxMSP and in the `Options' menu click `Audio Status' and set the `Signal Vector Size' to 8.

\item Work in pairs and collect a kit from the lecturer.

\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Further Signal Conditioning}

\subsubsection{Automatic Calibration}
Previously we have learned to calibrate or shift and scale sensor readings such that they operate within a desired range using the breakpoint function editor. It is generally useful to use the range 0. to 1. where possible so that inputs may be easily swapped. MaxMSP provides a number of similar objects that perform this scaling and shifting calculation one of which is called \texttt{[scale]} and another called \texttt{[zmap]} as shown in the image below:

\begin{center}
\resizebox{0.30\textwidth}{!}{\includegraphics{images/zmap}}
\end{center}

\texttt{[zmap]} has four arguments which set the \textit{input minimum}, \textit{input maximum}, \textit{output minimum}, \textit{output maximum}. It then scales all numbers received at its input into the specified output range. The object performs all the necessary scaling and shifting for you. Over the next two exercises we will now develop a sub patch that will provide an easy way to automatically calibrate our input signals for us.

\subsubsection{Exercise - Shifting and Scaling with \texttt{[zmap]}}
Set up a circuit to take readings from the flex sensor - use a pull up resistor arrangement such that the $V_{out}$ voltage reading goes up when the bend sensor is flexed:

\begin{center}
\resizebox{0.25\textwidth}{!}{\includegraphics{images/pullup}}
\end{center}

Observe the readings using a plotter and note down the minimum and maximum values. Now insert a \texttt{[zmap]} that shifts and scales the sensor readings into the range 0.0 to 1.0.

\subsubsection{Exercise - Automatic Calibration}
Now extend the patch so that it includes a full calibration procedure. That is, when a toggle box is checked the flex sensor can be flexed a number of times. Then, when the toggle box is unchecked, the minimum and maximum readings that were received since the toggle box was checked are sent to the 2nd and 3rd inputs of the \texttt{[zmap]} object to specify the input range. Here are a few pointers.


\begin{enumerate}
\item A \texttt{[peak 0.]} sends from its outlet the maximum number received at its left inlet.
\item A \texttt{[trough 1.]} send from its outlet the minimum number received at its left inlet.
\item A \texttt{[select 1]} can be used to identify when a toggle box has been checked (so that \texttt{[peak]} and \texttt{[trough]} can be reset).
\item A \texttt{[gate]} object a can be used to pass or block an incoming messages as shown below:
\begin{center}
\resizebox{0.20\textwidth}{!}{\includegraphics{images/gate}}
\end{center}
\end{enumerate}

When the \texttt{[toggle]} is checked, reset the \texttt{[peak]} (right inlet to 0.) and the \texttt{[trough]} (right inlet to 1.) objects and allow their outputs to flow through the gate to 2nd and 3rd inlets of the \texttt{[zmap]} object.

When you are happy that the calibration is working encapsulate the objects into a single subpatch so that it is easy to copy between patches as follows:
\begin{center}
\resizebox{0.40\textwidth}{!}{\includegraphics{images/calibration}}
\end{center}

\subsubsection{Plucking a Flexsensor}
Open the patch \texttt{String.maxpat} and click the button and move the slider to control a simple Karplus-Strong-based physical model. A floating point number on left inlet controls how hard the string is plucked and the slider on the right inlet controls the length of the string. We will now use the flex sensor to pluck the string. You will need to move plotting and calibration objects into the top region of the \texttt{String.maxpat} to begin with. Place the flex sensor over the edge of the table (like a ruler) and calibrate by toggling the calibration box and plucking the flex sensor a few times. Now observe how the calibrated input range varies as you pluck.

\subsubsection{Exercise - Simple Pluck}
Using a comparative operator object \texttt{[>]} choose a value as a threshold to trigger the string synthesiser. Think about whether the flex sensor should be triggered as the flex sensor reading rises above or drops below this value?

You will find that the string is triggered every time a new reading is received in this situation we are only interested to see when the output state from the \texttt{[>]} object changes. To prevent multiple triggers, insert a \texttt{[change]} object which only outputs a value when the value at the inlet changes.

You should find that the flex sensor behaves a bit like a string when you pluck it. However, you will find that as you flex the sensor so that the reading is exactly the same as the threshold value the noise will cause it to trigger multiple times. The diagram below shows a noisy flex sensor reading being plucked twice and should go some way to demonstrate why this might be:

\begin{center}
\resizebox{0.70\textwidth}{!}{\includegraphics{images/thresh}}
\end{center}

\subsubsection{Exercise - Adding Hysteresis}
As discussed in the lecture this week, hysteresis helps to reduce this rapid switching by placing a gap between the on and off threshold as shown below:

\begin{center}
\resizebox{0.70\textwidth}{!}{\includegraphics{images/2thresh}}
\end{center}

Modify your patch such that it uses two thresholds to trigger the string and ensure that the re-triggering is reduced when the bend sensor is flexed around the threshold.

\subsubsection{Exercise - Adding Expression}
Regardless of how hard the flex sensor is plucked, the velocity of the string sound is always the same. We will now extend the exercise such that when the on threshold is crossed a \texttt{[peak 0.]} object is reset and the maximum value from the flex sensor is used to set the amplitude of the sting pluck.

\subsubsection{Exercise - Controlling Pitch with an IR Sensor}
Connect the sharp IR sensor to control the pitch of the string as you have done in previous practicals:

\begin{center}
\resizebox{0.40\textwidth}{!}{\includegraphics{images/ir}}
\end{center}

You should again use a copy of your calibration sub patch to calibrate the range sensor properly.

\subsection{Digital Switches}
In the kit there are a number of tactile push switches:
\begin{center}
\resizebox{0.6\textwidth}{!}{\includegraphics{images/switch}}
\end{center}

When the button is pressed the two legs at the front become connected and the switch is closed.

\subsubsection{Exercise - Connecting a Switch}
Connect a switch to x-OSC as follows and use your plotter (as used in previous weeks) to view how the voltage on the channel 1 pin varies when the button is pressed.

\begin{center}
\resizebox{0.8\textwidth}{!}{\includegraphics{images/buttonCircuit}}
\end{center}

\subsection{Emulating Key Strokes}
Open the patcher \texttt{KeyStrokes.maxpat} in here you have a special object called \texttt{aka.keyboard} which emulates keystrokes - it emulates typing QWERTY keys on the keyboard. Open the application \texttt{TextEdit} and open a new text file. Now return to the Max patch and check the toggle box and then return to the text document you should see that the letter `m' is typed every second. Return to the Max patch and stop it! Also in this patch is the \texttt{[key]} object which displays the key code for any keys that are pressed.

\subsubsection{Exercise - Making a Game Controller}
From the patch you will see how to connect switches/buttons to emulate keystrokes. Make a circuit with three buttons/switches connected to three inputs on x-OSC. When these buttons are pressed they should be mapped to the left, right and space keys so that you can play a game using your breadboard controller. The key codes for left right and space are as follows:

\begin{itemize}
\item Left Key - 123
\item Right Key - 124
\item Space Key - 49
\end{itemize}

Then you can play space invaders \texttt{http://www.freeinvaders.org/}.

\subsubsection{Exercise - Electronic Ocarina}
Also provided this week is a patch called \texttt{Wind.maxpat} which contains a physical model of a wind instrument. There are three controls - the keyboard controls the pitch of the model and the `breath' controls the air pressure going into the model. If you want to send a pulse of air pressure when the keyboard is pressed toggle the check box.

Using four buttons and the flex sensor you should make an electronic ocarina. Construct the circuitry so that the four buttons are configured at the corners of a square on the breadboard, and then using white tack, attach the flex sensor to the side of the carrier board so that when you blow it it bends, you may need to attach a small piece of paper to the flex sensor to act like a sail. See below.

\begin{center}
\resizebox{0.6\textwidth}{!}{\includegraphics{images/ocarinaPhysical}}
\end{center}

You will also have to aggregate the button presses and convert their readings into single note numbers so that the fingering to note mapping is as shown below:
\begin{center}
\resizebox{0.9\textwidth}{!}{\includegraphics{images/ocarinaFingering}}
\end{center}

The patch \texttt{binaryRepresentation.maxpat} might help you to work out how to map switch inputs to the note numbers.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finishing up}

Make sure that you put all the equipment back in the box neatly and return it to the lecturer.
