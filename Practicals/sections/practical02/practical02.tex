\rule[.4cm]{\textwidth}{2pt}
\section{Practical 2 - Visualising and Understanding Sensor Readings}
\label{sec:prac02}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this week's practical session we will continue to build circuits that allow us to use sensors in the kit to control audio parameters in MaxMSP. We will introduce a new sensor and we will also extend our knowlege of Max to build data visualisation tools that will helps us to better understand and use the sensor data available. We will also begin to use basic forms of signal conditioning to derive a binary state from an analogue voltage reading. By the end of this practical you should understand:

\begin{enumerate}
\item how to make a simple plotter in Max.
\item the properties and characteristics of the sensors we are using.
\item how to use a threshold in your Max patch to use a sensor as a switch.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Getting Started}

\begin{enumerate}
\item Find your \texttt{\textbf{/MobileAndPhysicalComputing}} folder on your Network storage space.

\item Now navigate to the folder \\\texttt{Desktop/StudentShared/fet/CSCT/MobileAndPhysicalComputing-UFCF9G-30-2/} and copy the entire folder \texttt{\textbf{Practical02}} to your  \texttt{\textbf{/MobileAndPhysicalComputing}} folder.

\item Work in pairs and collect a kit from the lecturer.

\begin{center}
\resizebox{0.4\textwidth}{!}{\includegraphics{images/SliderXosc}}
\end{center}


\end{enumerate}

\subsection{Data Visualisation}

When working with physical computing systems, the tools that we use to observe and interpret sensor readings are very critical - visualising how readings fluctuate with respect to time help us to interpret this data and apply it most effectively.

\subsection{Activity Monitor}

From \textbf{your} copy of the \texttt{MobileAndPhysicalComputing-UFCF9G-30-2} folder navigate to \texttt{Practical02} and double click the Max patch \texttt{MidiMessageCounter.maxpat}. You should see the following patch:

\begin{center}
\resizebox{0.8\textwidth}{!}{\includegraphics{images/messageCounter}}
\end{center}

In the `MIDI Input Device' drop down box, select ``Impulse'' and note how when the keys, sliders, pads and other controls are used, the MIDI activity and its message type is shown with the use of button objects to indicate when messages are received. The patch also uses each message to advance a message counter which indicates how many messages have been received since the patch was opened.

When working with input devices, it is important to understand when data is being sent, whether the activity is continuous or sporadic, and the rate at which data is sent/received. The latter of these involves calculating how many readings per second are being received. In Max this is relatively simple to compute, but requires a little thought.

\subsubsection{Exercise - Message Rate Counter}
Modify the \texttt{MidiMessageCounter.maxpat} so that the number of messages received every second (the message rate) is indicated. The message rate should be updated once per second and should always indicate the number of messages received in the last second.

To achieve this you will need to remember how to:
\begin{enumerate}
\item generate a bang message every second.
\item respond to this bang message in two ways:
\begin{enumerate}
\item Display the current value of the counter (the number of messages received in the last second) in number or message box.
\item \textbf{Afterwards}, this bang message should also be used to reset the counter.
\end{enumerate}
\end{enumerate}

Notes:
\begin{itemize}
\item Recall how message ordering works in Max (right-to-left, bottom-to-top).
\item Remember that the trigger ([t]) object enables you to control the message ordering explicitly, which is preferred to using positional (right-to-left, bottom-to-top) message ordering.
\item The right inlet of a message box enables you to store and display a message without outputting it.
\item The metro can be set to enabled on load with the \texttt{@active 1} argument.
\end{itemize}

\subsubsection{Exercise - Building A Message Rate Counter Subpatch}
When you have completed the exercise above, encapsulate the part of the patch that calculates the message rate into a subpatch. To do this, select the parts of the patch that calculate the message rate and select \texttt{Edit $\rightarrow$ Encapsulate}. The sub patch should have one inlet and one outlet, the outlet should indicate the number of messages received at the inlet in the last second. Subpatches are indicated with the prefix `\texttt{p}': Rename the sub patch to something meaningful like \texttt{[p messagerate'} so that it will be easier to identify and copy into future patches. Save the patch safely on your homespace.

\subsubsection{Exercise - x-OSC send rate measurements}
Use the message rate counter subpatch that you developed earlier to work out the rate at which x-OSC is sending messages. Make note of this number.

\subsubsection{x-OSC Settings} It is possible to change lots of x-OSC settings via a browser. Open a browser and navigate to the IP address \texttt{169.254.1.1} and you will find the configuration page for x-OSC. If the page is slow to load it will be because there are many other x-OSC's in the room interfering with each other, don't worry for now just give the page a couple of minutes to load. When the page has loaded, open the inputs section you can change the sample rate to 1 Hz and click the save button at the bottom you will notice that the Max patch should now only receive a message once every second. Change the input reading back to 20 Hz, which is fast enough for now - you can go up to 350 Hz, but \textbf{don't} set the rate any higher than 20 Hz, to prevent the airwaves from becoming clogged. Feel free to have a look around the rest of the settings, but don't change anything else at this stage.

\subsection{Data Plotter}
As music technologists, it is always helpful to look at waveforms and spectrograms in order to visually assess and understand sound. The same applies to control rate values: by plotting a history of control values we can interpret a great deal of information which can help us to understand the nature of the incoming data and to establish how it can be used or modified for our intended application.

As well as measuring the rate at which messages are received, it is also very helpful to plot a history of the data values to develop an understanding of how the data readings fluctuate over time or in response to physical interactions.

Return to the \texttt{Practical02} folder and double click the Max patch \texttt{DataPlotter.maxpat}. You should see the following patch:

\begin{center}
\resizebox{0.6\textwidth}{!}{\includegraphics{images/dataPlotter}}
\end{center}

Again in the `MIDI Input Device' drop down box, select ``Impulse'' and observe that when the pitchwheel on the Axiom is adjusted, the position of the slider reacts accordingly.

\subsubsection{Exercise - Building a Plotter}
The current version of the patch plots only the last pitch wheel value that was received, we would like to modify the patch to plot the history of values received. Modify the patch to replace the slider with a history plot. Follow the steps below to achieve this.

\begin{enumerate}
\item Replace the slider object with a new \texttt{[multislider]} object.
\item Resize the \texttt{[multislider]} object such that it extends to the right side of the patch widow (so that it has a `widescreen' aspect ratio).
\item Move the pitch wheel and observe that the slider only traverses half of the slider range.
\item Right click the \texttt{[multislider]} and select `Inspector'.
\begin{enumerate}
\item Change the `Range' to `\texttt{0. 1.}'.
\item Change the `Slider Style' to `Line Scroll'
\end{enumerate}
\item Now observe the plot when the pitchwheel is moved.
\end{enumerate}

%\subsubsection{Exercise - Continuous Plotting}
%You may have noticed that the plotter only plots when the pitch wheel is changed: the x-axis advances by one unit every time a new value is received. To plot continuously as a function of time, we will have to modify the patch so that a value is sent to the plotter every fixed time period (say every 100 ms). The value sent should always be the last value received from the pitchwheel.

%Think back to the message rate counter for a possible way to achieve this. When you have a working solution, add a number box that controls the rate of the plotter in Hz. When ready, encapsulate all of the objects that produce the plotting so that your patch appears as shown below:

%\begin{center}
%\resizebox{0.8\textwidth}{!}{\includegraphics{images/dataPlotterSolution.eps}}
%\end{center}
%%%
%%%
%%%%%%%%%%
%%%
%%%

\subsubsection{Exercise - x-OSC Input Plotting}
Now take the plotter that you developed earlier and plot the input value from the slide potentiometer. You should be able to view the history of the slider movement.

This is going to be very useful in future for observing the response of other sensors that we will be working with.

\subsubsection{Exercise - 3 Channel Plotter}
Build upon what you created in MaxMSP last week and make a 3 channel plotter that should simultaneously plot channels 1, 2 and 3 of x-OSC's inputs. Test that your patch works, by connecting the slider connection from x-OSC input channel 1 to input channel 2 and then to input channel 3.

\subsection{Exercise - String Patch}
Open the patch in this week's folder called \texttt{String.maxpat} and click the button and move the slider to control a simple KarplusStrong-based physical model. A floating point number on left inlet controls how hard the string is plucked and the slider on the right inlet controls the length of the string.

Build on this patch so that the pitch or length of the string is controlled by a range sensor.

It would be helpful to build on this so that we can pluck the string via a second physical interaction... read on.

\subsection{New Sensor - Piezo Sensor}
\begin{center}
\resizebox{0.40\textwidth}{!}{\includegraphics{images/piezo}}
\end{center}

The piezo element has two leads, and when the pad is pressed or flicked the element generates a small voltage across its leads. Piezo elements come in lots of shapes and sizes, but the ones in your kit are well-suited for detecting a hit or finger press.

The only connections are a ground and signal, so you would need to connect the piezo to ground and to one of x-OSC's input channels and take a look at the plot as you press/tap the pad. If the reading does not appear very clear, reverse the polarity of the piezo sensor (swap the ground and signal connections) and see if the reading is improved.

\subsection{Exercise - Using a Threshold to Pluck the String}
We would like to now use the peizo like a switch so that when we tap the surface of the sensor, it `plucks' the string. So we need to make a trigger, when the sensor value crosses a certain threshold it should fire a bang message. To do this you should think about the relational operators that you learned in C programming last year (\texttt{>},\texttt{=<}, \texttt{==}, etc), as similarly named objects are also available in Max.

When you are happy that this is working, mount the piezo and distance sensor on the underside of the carrier board so that you can play the interface like guitar.

\subsubsection{Homework}
Make sure that you complete all the reading and review exercises that have been set so far. The reading can be found on the module blackboard page.
