\rule[.4cm]{\textwidth}{2pt}
\section{Practical 5 - Open Sound Control and LED Control}
\label{sec:prac05}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this practical we learn about Open Sound Control (OSC) and also the digital and PWM outputs on x-OSC. On successful completion of this practical you will have an understanding of:

%x-OSC
%OSC,
%OUTPUTS 
%-DIGITAL
%--Toggle box to LED - flash.
%--Slider with thresh to switch LED
%--Couple of LEDs - extend to level meter.

%-PWM
%--Slider to LED - flash slow - fast - PWM for fading.
%-- Three trimpots to RGB - Colour Picker in MAX to LED.
%-- Servo - sensor with limited range calibrate - get beaters from Pete! - level meter with servo. hitting things with solenoid. 

\begin{enumerate}
\item The use of OSC and the relevant \texttt{[udpsend]} and \texttt{[udprecieve]} objects in MaxMSP.
\item Simple LED circuits and how to drive them using the digital and PWM outputs.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Getting Started}

\begin{enumerate}
\item Find your \texttt{\textbf{/MobileAndPhysicalComputing}} folder on your Network storage space. 

\item Now navigate to the folder \\\texttt{Desktop/StudentShared/fet/CSCT/MobileAndPhysicalComputing-UFCF9G-30-2/} and copy the entire folder \texttt{\textbf{Practical05}} to your  \texttt{\textbf{/MobileAndPhysicalComputing}} folder.

\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Open Sound Control (OSC)}
Open Sound Control is a protocol for communication among computers, sound synthesisers, and other multimedia devices developed at the Center for New Music and Audio Technologies (CNMAT) in 1997 (Wright and Freed, 1997). 

OSC provides a network protocol which is ideal for interactive computer music applications as it can easily run over existing high-speed network technologies such as Ethernet and Wi-Fi. Today most OSC implementations use the internet protocol suite (TCP/IP) via Ethernet or Wi-Fi network connections; however, as OSC may be used over other types of network it is effectively `transport-independent'. 

In these sessions, we have been and will continue to use OSC to communicate with the x-OSC interface device; however, it may also be used to easily communicate with any other device on the network. 

\subsubsection{OSC Over the UWE Network}
We will first use OSC to send messages between the computers in the lab as they are all connected to the same network. For this exercise you will work with the group next to you as you will be exchanging messages between each others' Max patches. 

\subsubsection{Internet Protocol (IP) Addresses}
To send and receive OSC messages you need to know your computer's local IP address, which is a unique number that is allocated to your machine so that it can be addressed as an individual member of the network. You can find your IP address by clicking on the Wi-Fi menulet icon of the OSX menubar and select `Open Network Preferences' and, on the `Network' settings window that appears, click on the Ethernet item in the left hand menu and read/note the IP address from the `Status' section of the window - the IP address is represented as four numbers separated by full stops, note that each of these four numbers is in the range 0 - 255 (8-bit). It is likely to be something like: \texttt{164.11.13.X}, where \texttt{X} is a number between 0 - 255.

\begin{center}
\resizebox{0.4\textwidth}{!}{\includegraphics{images/menulet}}
\end{center}

\subsubsection{Exercise - Exchanging OSC Messages}
Work with the group next to you for this exercise and name your groups `Group 1' and `Group 2'. First we will use the OSC objects within Max to send messages from Group 1's computer and receive them on Group 2's. To send messages to Group 2, you need to know the IP address of their computer; from your computer's perspective this is referred to as the \textit{remote IP address}.

\begin{enumerate}
\item on Group 1's machine, open Max and in a new patcher and create a \texttt{[udpsend]} object and set two arguments: 
\begin{itemize} 
\item the first should be the IP address of Groups 2's machine e.g. the text \texttt{164.11.13.X} (replacing X with the actual number provided by Group 2), 
\item the second argument should be port number 8000 on which we will send our OSC messages. 
\end{itemize} 
\item now check that there were no errors in the Max window and then connect a single message box to the inlet of \texttt{[udpsend]} and set the message to be \texttt{/test}, as shown in the example below.

\begin{center}
\resizebox{0.35\textwidth}{!}{\includegraphics{images/udpsend}}
\end{center}

\item Now on Group 2's machine create an object called \texttt{[udpreceive]} with a single argument to specify the port on which to receive messages from Group 1. This must match the port that we specified earlier: 8000.
\item Connect a \texttt{[message]} object to display the received messages as shown below. Note that we have connected to the right inlet of the \texttt{[message]} box and that in this configuration nothing will be sent from the outlet of the message box unless it is clicked with a mouse.

\begin{center}
\resizebox{0.3\textwidth}{!}{\includegraphics{images/udpreceive}}
\end{center}

\end{enumerate} 

Now repeat this process to send messages from Group 2's computer to Group 1, you should use a different port for this, perhaps 9000. 

\subsubsection{OSC Message Format}
The fundamental units of OSC are messages and all we need to understand is that each valid OSC message has a \textbf{compulsory} \textit{address pattern} and an \textbf{optional} list of \textit{arguments}. The \texttt{/test} message that you sent to your neighbouring group is a valid OSC message where \texttt{/test} is the address pattern and there were no arguments. Other messages can include arguments, for example in the message:

\begin{center}
\texttt{/oscillator/frequency 440.}
\end{center}

the address pattern is `\texttt{/oscillator/frequency}' and `\texttt{440.}' is a single floating-point argument. In the message:

\begin{center}
\texttt{/oscillator 440. 1. saw}
\end{center}

the address pattern is `\texttt{/oscillator}' and `\texttt{440. 1. saw}' are three arguments where the first two are floating-point arguments and the last is a string argument. 

\subsubsection{OSC Address Patterns}
All OSC messages \textbf{must} begin with an address pattern, which is a readable string of text that begins with a / (forwards slash). Address patterns can be expanded with additional slash separated text to form a hierarchal structure that can be neatly organised to resemble a web address or file system path. For example, if you are sending OSC to a synthesiser and you want to begin its demo sequence you may define the address pattern:
\begin{center}\texttt{/synth/beginDemo}\end{center} 
which could be sent to a synthesiser to invoke a demo routine. This is just an example that I made up just now, like the \texttt{/oscillator} example above. The beauty of OSC, is that it allows \textit{you} to define and agree your own address patterns with the receiver of your own OSC messages, so it is best to choose address patterns that best describe the data that is encoded into the arguments. If MIDI were used as an alternative, you would be forced to cram your messages into the existing message types, which is fine for note/music information, but other types of data (sensor readings etc.) must mapped onto the existing message types (notes, control change messages, etc.), which can be confusing.  

\subsubsection{OSC Arguments}
Following the address pattern, optional arguments may also be specified to send specific parameters and values across the network the limit to the number of arguments is very high (in the region of ~350). OSC supports a huge range of different message types, but the most common and useful types are integers, floating-point numbers and strings. In Max, you can add arguments to an OSC message by simply adding them after the address patterns separated by a space to form a list. For example, here is another made up example to illustrate this point - to set the frequency of an oscillator, on the host device, you may send to it a message as follows:

\begin{center}
\resizebox{0.3\textwidth}{!}{\includegraphics{images/arguments}}
\end{center} 

Multiple arguments of differing types can also be sent:

\begin{center}
\resizebox{0.3\textwidth}{!}{\includegraphics{images/arguments2}}
\end{center} 

OSC message arguments can also be constructed from number boxes as follows:

\begin{center}
\resizebox{0.3\textwidth}{!}{\includegraphics{images/arguments3}}
\end{center} 

If you are not clear what the \texttt{[pack 0 0]} object does or what the \texttt{\$1} and \texttt{\$2} are doing in the message box, try to work this out and ask a lecturer if you are unsure.

\subsubsection{Exercise - Making Up New OSC Messages}
With your connection to the other group, make up three or four valid OSC messages to send to the other group. Remember, to be valid, these messages must be written in MaxMSP to the \texttt{[udpsend]} object as a list beginning with a valid address pattern followed by any number of integer, floating-point and/or string arguments. 

\subsubsection{Exercise - Playing MIDI Over the Network}
Work with the other group again to make a patch that allows you collaborate playing your Axioms over the network. All MIDI note on and off messages should be sent to the other group's workstation via OSC where it should be plugged into the MaxMSP `AU DLS Synthesiser' to allow you to play duets over the network. You should then be able to hear yourself and the other group play together on your headphones. You could be in different rooms in N-Block and this patch would still work.

First, make sure that you can play the `AU DLS Synthesiser' from your own workstation. For this you will need to use the \texttt{[midiin]} and \texttt{[midiparse]} objects. You should then connect the note messages from \texttt{[midiparse]} object to the \texttt{[noteout]} object. Make sure that the \texttt{[noteout]} object is sending messages to the `AU DLS Synthesiser' and enjoy a moment of piano.

Now the important information that we need to send to the other group is the note number and velocity. We have a list for each note message from \texttt{[midiparse]} so in addition to connecting these numbers to \texttt{[noteout]} also send them as an OSC message to the other group. \textbf{Remember}, to be a valid OSC message these arguments (note number and note velocity) should be preceded with an OSC address pattern. To achieve this, remember the image above where a message box is shown above with the \texttt{\$1} and \texttt{\$2} symbols. 

OSC could enable you to collaborate across the entire campus or over the internet.
 
\subsection{x-OSC - Outputs and LEDs}
We will now start to use the x-OSC output connections to enable our programs to produce visible output that can provide feedback about the state of our Max patch without having to look at the screen - very important for building physical computing devices. Collect a kit from the lecturer if you don't have one already.

In the kit you will find a number of LEDs, there are 5 green, 5 red, 5 yellow and a single (big) red, green and blue (RGB) LED. You can't tell the small green, red and yellow LEDs apart so pick one out and we will shortly find out. 

\subsubsection{Diodes}
LED stands for ``Light Emitting Diode'' and represents the name given to a type of diode that converts electrical current into light. In electrical circuits it is represented as shown in the diagram below:

\begin{center}
\resizebox{0.3\textwidth}{!}{\includegraphics{images/diode}}
\end{center}

\subsection{LED Curcuits}
LEDs, like all diodes, only allow current to flow in one direction and when there's no current there's no light. So if you connect it the wrong way around there will be no light. 

The positive side of the LED is called the \textit{anode} and is marked by having a longer leg. The other, negative side of the LED is called the \textit{cathode}. Current flows from the anode to the cathode and is blocked in the opposite direction. See the diagram below:

\begin{center}
\resizebox{0.25\textwidth}{!}{\includegraphics{images/led}}
\end{center}

When configured so that current flows from anode to cathode, the diode has negligible resistance, so if an LED is connected directly to a power source it will draw lots of current and eventually burn up. To prevent this, we limit the current with a 330 $\Omega$ resistor in series with the diode as shown in the diagram below.

\begin{center}
\resizebox{0.8\textwidth}{!}{\includegraphics{images/ledfritz}}
\end{center}

\subsection{Exercise - Find the LEDs?}
Connect up the circuit as shown. Be very careful that you do not to connect 3.3 V directly to ground. Find one red, one green and one yellow LED.

\subsection{Exercise - Controlling the LED}
Remove the wire connecting the x-OSC 3.3 V connection to the breadboard as we will not be needing it. Now wire the circuit so that the x-OSC Output 1 is connected to drive the LED circuit as shown below:
\begin{center}
\resizebox{0.6\textwidth}{!}{\includegraphics{images/ledfritz2}}
\end{center}

Now in MaxMSP create a \texttt{[udpsend]} object and send to the x-OSC IP address: 169.254.1.1 on port 9000. To set the output 1 to digital state 1 (3.3 V), send the OSC message:
\begin{center}
\texttt{/outputs/digital/1 1}
\end{center}

To turn the LED off again, set the argument to 0. Set the patch up so that a toggle box is used to control the LED state. Now extend again so that the linear slider is used to control the LED such that when the slider is in the top half of its travel the LED is on and in the bottom half it is off. 

\subsection{Exercise - An LED Meter}
Open the patch in this week's folder called \texttt{meter.maxpat} here you will see a simple file player and a meter, load the \texttt{BeltDriveBreaks.wav} file given out in Practical 03. Check that the meter produces an output value at the shown interval. Build a metering circuit and patch that uses 5 or more LEDs where a higher number of LEDs are illuminated when the meter reading is higher. The circuit for connecting two LEDs is shown below, you will have to extend this for 5 or more LEDs:

\begin{center}
\resizebox{0.6\textwidth}{!}{\includegraphics{images/ledfritz3}}
\end{center}

\subsection{Pulse Width Modulation}
To control the brightness of an LED we can used Pulse Width Modulation or PWM. PWM signals are a square wave oscillation with controls for the waveform frequency and duty cycle. Frequency should be well understood now, but duty cycle may be a new concept. Duty cycle controls the ratio of on time to off time in the wavefrom (sometimes called the mark space ratio). At high frequency, the duty cycle controls the amount of current that passes through an LED and therefore controls its brightness. Different duty cycle values are shown in the diagram below:

\begin{center}
\resizebox{0.8\textwidth}{!}{\includegraphics{images/PWMPlots}}
\end{center}

We will now access the x-OSC settings page so that we can control the PWM settings. Open Safari, and browse to x-OSC webpage at the address \texttt{169.254.1.1}. If you encounter difficulties loading this page, unplug the network cable from the computer that you are working on and reconnect it as soon as you are done changing the settings. When the settings page has loaded, scroll to and open the outputs section and set channel one to PWM with a frequency of 5 Hz and a duty cycle of 50 \%. Now change the duty cycle from 10 \% to 90 \% to observe its effect. This duty cycle can also be set by sending the following OSC message to x-OSC:

\begin{center}
\texttt{/outputs/pwm/duty/1 50}
\end{center}

Where 50 here sets the duty cycle to 50 \%, change the argument to 10 and then 90 (the range is 0 to 100). It is also possible to change the frequency of the PWM signal with the OSC message:

\begin{center}
\texttt{/outputs/pwm/frequency/1 50}
\end{center}

The range of the frequency is 5 - 250000 Hz.

Now use a Max slider to control the duty cycle of the signal and you should find that you have continuous control over the brightness of the LED.

\subsection{RGB LED}
Also in your kit is an RGB LED which is pictured below. This LED actually contains 3 LEDs, one red, one green and one blue with a common ground for all pins. 

\begin{center}
\resizebox{0.3\textwidth}{!}{\includegraphics{images/RGB}}
\end{center}

Pin 2 is the ground connection, Pin 1 is Red, Pin 3 is Green and Pin 4 is Blue. A circuit that could be used to drive the RBG LED is shown below referencing the pin numbers shown above. 

\begin{center}
\resizebox{0.5\textwidth}{!}{\includegraphics{images/RGBCircuit}}
\end{center}

\subsubsection{Exercise - RGB Led}

Open the patch \texttt{ColourPicker.maxpat} and see how the red green and blue colours are output as separate values. Map these values onto the pins of the RGB led so that you can display any colour on the LED.  Now make an additional metering program that uses the single RGB LED to fade from green to red as the level of the audio increases.

\subsection{Finishing up}
Make sure that you put all the equipment back in the box correctly and return it to the lecturer. 
