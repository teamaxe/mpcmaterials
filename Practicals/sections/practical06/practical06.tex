\rule[.4cm]{\textwidth}{2pt}
\section{Practical 6 - Motion Control and Gesture Detection}
\label{sec:prac06}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this practical you will learn how to capture and process motion using the 3-dimensional accelerometer and gyroscope sensors on the x-OSC board and also an external 3-dimensional accelerometer. By the end of this session you should have understanding of:

\begin{enumerate}
\item how to receive and decode OSC messages from x-OSC (beyond what was covered last week).
\item how to calibrate and interpret acceleration data.
\item how to combine and compute pitch and roll from 3D accelerometer readings.
\item how to read gyroscope readings to measure rotation.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Getting Started}

\begin{enumerate}
\item Find your \texttt{\textbf{/MobileAndPhysicalComputing}} folder on your Network storage space. 

\item Now navigate to the folder \\\texttt{Desktop/StudentShared/fet/CSCT/MobileAndPhysicalComputing-UFCF9G-30-2/} and copy the entire folder \texttt{\textbf{Practical06}} to your  \texttt{\textbf{/MobileAndPhysicalComputing}} folder.

\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Wearable Motion Sensing}

With the proliferation of low-cost, high-performance, miniaturised motion sensors as a result of the recent advancements in mobile phone technology, motion sensing technology has become increasingly available. The most common of these motion sensing devices are microelectromechanical systems or MEMS devices, in which actual mechanical microsensors are embedded on an integrated circuit or chip. In your kit there are a number of MEMs sensors. x-OSC has a built in 3-axis accelerometer and a 3-axis gyroscope as shown below, note the orientation of the axes with respect to the board: 

\begin{center}
\includegraphics[width=0.4\textwidth]{images/xoscAG}
\end{center}

You will also find a small red circuit board fitted with with a triple axes accelerometer called an ADXL335. 

\begin{center}
\includegraphics[width=0.235\textwidth]{images/accelerometer}
\end{center}

The ADXL335 and the onboard x-OSC accelerometer both measure acceleration along the direction of three axes indicated in the images above and the readings indicate both the force due to gravity and any additional forces as a result of motion. Linear acceleration felt by the sensor in the direction of one of these axes will result in a positive reading. Note that the dot within a circle for the Z direction in the images above indicates that this axis points upwards out of the circuit board. 

\subsubsection{Exercise - Reading Acceleration}
We will begin with the readings from the ADXL335, which is powered from a 3.3 V supply and provides three independent analogue outputs for the x, y and z axes measurements. Connect the accelerometer to the x-OSC as shown in the diagram below. Now open a new Max patcher window and build a patch to decode and separate the analogue readings from channels 1, 2 and 3 (x, y and z respectively). 

\begin{center}
\includegraphics[width=0.65\textwidth]{images/accelerometercircuit}
\end{center}

You will notice that as you rotate the entire orange carrier the acceleration readings are quite small. This is because this accelerometer is able to sense positive and negative acceleration in the rage $\pm$5 g on each axis - five times the force due to gravity - which is mapped onto a reading in the range 0 to 1 (where 0 is $-$5 g 1 is $+$5 g).

\subsubsection{Exercise - Calibrating Acceleration Readings}
It would be desirable to calibrate our accelerometer to view its readings in the units of g. This can be achieved simply by scaling the incoming reading in the range 0. to 1. to the desired range $-$5 to $+$5 using an \texttt{[expr]} object. Build a patch that uses this method to calibrate the x, y and z axis and then plot view and plot the readings, as you rotate and move the carrier board.

While this method is simple it is not the most accurate. A more accurate calibration can be achieved using the calibration method that we developed in practical 4 (which you will hopefully have in a sub patcher in a solution to the exercises in practical 4). Using this method we can calibrate by finding the input minimum and input maximum values, which can be obtained by placing each axis such that it is aligned positively with the force of gravity (for the max reading) and negatively with the force of gravity (for the min reading). With your orange carrier flat on the table, the force due to gravity should read 1 g along the z-axis of the accelerometer (and zero on the other axes), if the orange carrier were upside down, the z-axis accelerometer reading would be -1 g. Use your calibration procedure developed in practical 4 to obtain these readings. Note that as the \texttt{zmap} object will cap the readings at $\pm$1 g you should use the \texttt{[scale]} object instead, which is identical, only it allows readings to extend beyond the min and max values giving us the full $\pm$ 5 g range. Give this process a go based on the work completed in previous practicals before asking for help.

When you have completed this process, repeat this calibration for all three axes to produce a fully calibrated 3D accelerometer. Add a simple plotter for each axis using the \texttt{[multislider]} object and plot the full range for the accelerometer $\pm$5 g. Move the carrier board to establish the link between the observed readings and movements. 

\subsection{Accelerometer Signal Conditioning}
Accelerometer readings can be interpreted to extract a range of different motion, the Nintendo Wii Remote is an outstanding exemplar of how rich computer games interaction can become with the use of these sensors. For musical purposes, there are a number of different processes that we can apply to the calibrated accelerometer readings. For example, we will now process the data in two forms, firstly to detect drum gestures and secondly to compute the tilt angles (pitch and roll).

\subsubsection{Exercise - Drum Detection}
With all three axes of the accelerometer calibrated to give readings in terms of g, pick up the orange carrier and hold it such that the y-axis points towards the ceiling. At this orientation, the y-axis should measure 1 g. Be carful now - make sure that you don't drop the carrier board. Make a few air drum gestures and, on the plotter, observe the fluctuations in the data - you will see that as your hand travels towards the floor the reading drops and as you strike the virtual drum there is a big positive value spike. 

Use a threshold to trigger \textbf{only} one bang message when the threshold is crossed. Now, open the patch \texttt{drums.maxpat} and click the button, if the audio is enabled you will find that a bass drum sound is triggered. Move these components into the patch with the accelerometer readings and use the threshold detection to trigger the bass drum. Extend the patch so that similar spikes along the positive or negative directions of the other axes trigger other drum sounds.

\subsubsection{Tilt Control}
If we assume for a moment that gravity is the only force acting on the accelerometer (it is not accelerating in any other directions) it is possible to calculate the tilt of the accelerometer in terms of its roll ($\phi$) and pitch ($\theta$) angles (see image below) by the location of the force of gravity along the x-, y- and z-axes. 

\begin{center}
\includegraphics[width=0.5\textwidth]{images/tilt}
\end{center}

The trigonometry for calculating the roll ($\phi$) and pitch ($\theta$) angles as follows:
\begin{equation}
\tan \phi = \frac{a_y}{a_z}
\end{equation}

\begin{equation}
\tan \theta = \frac{a_x}{\sqrt{{a_y}^2 + {a_z}^2} }
\end{equation}

where $a_x$, $a_y$ and $a_z$ are the acceleration measurements for x, y and z axes respectively. The angles can therefore be calculated in degrees using the following mathematical expressions:
%Roll = atan2(Y, Z) * 180/M_PI;

%Pitch = atan2(X, sqrt(Y*Y + Z*Z)) * 180/M_PI;

\begin{equation}
Roll = atan2(a_y, a_z) \times \frac{180}{\pi}
\end{equation}

\begin{equation}
Pitch = atan2(a_x, \sqrt{{a_y}^2 + {a_z}^2}) \times \frac{180}{\pi}
\end{equation}

\subsubsection{Exercise - Tilt Control}
Look up and use the \texttt{[expr]} object to perform these calculations and extract the pitch and roll angles. Note that \texttt{atan2()} and \texttt{sqrt()} are valid \texttt{[expr]} functions, however there is no symbol for $\pi$ so you will have to manually type the constant (3.14159). When you are ready, check that these readings appear correct by tilting the orange carrier. Now map these angles to control the frequency and amplitude of a simple sine wave oscillator. Note that the signals will sound a bit wobbly, add smoothing to the x, y, and z axis readings (before the tilt calculation) to make this more controlled.

\subsubsection{Exercise - Mouse Control}
Create a wireless mouse controller using the \texttt{[aka.mouse]} object bundled with this weeks practical. Sending the \texttt{[aka.mouse]} object the message \texttt{moveto 0 0} will result in the mouse moving to the top left corner of the screen, coordinate 0 0. The units of the arguments are in pixels, so sending the x y resolution of your monitor will move the mouse to the bottom right corner of the screen. You can find your screen resolution with the \texttt{[mousestate]} object: 

\begin{center}
\includegraphics[width=0.4\textwidth]{images/mousestate}
\end{center} 

Map the pitch and roll readings to control the mouse position. Also add two buttons to the circuit so that the mouse can be left and right clicked. Note that sending the message \texttt{click} to \texttt{[aka.mouse]} triggers a click and the message \texttt{click 1} triggers a right click. Make sure that the mouse position is smoothed. 

\subsection{x-OSC Gyroscope Readings}
As well as including an accelerometer the x-OSC also includes a gyroscope which measures rotation around the x, y and z axes. The measurements are in $^{\circ}$ per second in the range $\pm$2000. So if you rotate at 1 RPM you will see a reading of 360 on the axis of rotation.

\subsection{Exercise - Extracting - Gyroscope Readings}
To get these readings we need to modify the settings on x-OSC so that as well as sending the \texttt{/analogue/inputs} message, x-OSC also sends \texttt{/imu} messages. IMU stands for inertial measurement unit and the arguments to this message indicate the current motion sensor readings. 

In a browser, navigate to the x-OSC webpage at the IP address \texttt{169.254.1.1} and scroll down and open the `IMU' section and set `Sensor send rate' to 20Hz. You should find that x-OSC is now sending the \texttt{/imu} message as well as the \texttt{/analogue/inputs} reading. You will have to use the \texttt{[route]} object to separate these two messages. 

The first three of the ten arguments to \texttt{/imu} message are the gyroscope x, y and z readings. Notice that on diagram 1 the z axis would measure rotation as the x-OSC carrier board is rotated on the table. Extract the z axis reading and use the \texttt{[expr]} object to scale the $\pm$2000 $^{\circ}/s$ reading to the range 0 to 1 and feed the value to a plotter as you rotate the carrier board.

\subsection{Exercise - GyroScratch}
We will now make x-OSC into a virtual turntable. Open the patch \texttt{FilePlay.maxpat} provided in this week's files. When the audio is enabled the audio track should play, and the playback rate can be controlled by the left slider in the range $\pm$5. Use the gyroscope reading to control the playback rate so that when the x-OSC is rotated clockwise the audio track plays forwards and, and conversely when the x-OSC is rotated anticlockwise. Your scaling will control the mapping from x-OSC RPM to playback rate. Perhaps unplug the x-OSC and rotate it on a swivel chair to get a smooth playback, or alternatively spin on the spot.

\subsection{Exercise - Rotary Encoder}
The gyroscope can also be used as a rotary encoder to increase or decrease values, so that x-OSC becomes like a controller wheel. This is achieved by accumulating the values received by x-OSC is simple to do in Max, the method is shown below. Change the patch above so that the pitch is controlled like a rotary encoder.

\begin{center}
\includegraphics[width=0.2\textwidth]{images/MaxAccumulator}
\end{center} 

\subsection{Exercise - Two handed drums}
The x-OSC also sends accelerometer readings with the \texttt{/imu} message. The 4th, 5th and 6th arguments are the accelerometer x,y and z readings. Unlike the ADXL335 x-OSC already provides it's readings in g so there's no need to calibrate the readings. Create a two handed air drum kit using the accelerometers on the x-OSC and ADXL335 with one device in each hand. You will need to use an extension cable so that the x-OSC in one hand can connect to the ADXL335 in the other. Provided in the kit are ribbon cables where you can insert the ADXL335 as shown in the picture below:

\begin{center}
\includegraphics[width=0.4\textwidth]{images/adxlExtend}
\end{center} 

Make sure that you do not mix up any wiring.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finishing up}
Make sure that you put all the equipment back in the box correctly and return it to the lecturer. 


