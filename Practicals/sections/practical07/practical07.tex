\rule[.4cm]{\textwidth}{2pt}
\section{Practical 7 - Short Project}
\label{sec:prac07}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this practical we will build an interface resembling an instrument which will combine much of the work completed in previous practicals. The instrument will also implement a simple state machine to map a small number of input controls to a number of different sound generating parameters (one-to-many mapping). By the end of this practical you will be able to: 

\begin{enumerate}
\item integrate much of the work from previous sessions.
\item map MIDI messages from Max/MSP to control a synthesiser in Logic Pro.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Building an Expressive Trumpet Controller}
Throughout the subsequent sections, we will build an electronic trumpet-inspired controller and map the controls to a trumpet synthesiser in Logic Pro. It's important to note that when designing this practical the starting point was trumpet synthesiser found in Logic, and then the design of the interface including the electronics, signal conditioning and mappings are all inspired by thinking about what the controller for this particular synthesiser should be like.

\subsubsection{Exercise - Keys}
The controller will include 3 buttons that will switch the note that the controller will play, different combinations should trigger a different note. Refer to practical 4 to remind yourself how to connect switches to an analogue input. 

Now we will map the three keys onto MIDI note numbers as follows, note that these are not true trumpet key mappings, but they are a compromise based up on the correct trumpet key mappings and the interface equipment available to us:

\begin{center}
\includegraphics[width=1.0\textwidth]{images/trump}
\end{center}

You will need to convert from the three input channels (switches) to seven MIDI note numbers. We will interpret the three buttons as if they are binary digits, so that pressing them is like setting the bits of a three bit binary number to construct a single integer. There are a number of ways to achieve this which are shown in the images below. Options (a) and (b) construct the number using knowledge of binary representation, option (c) uses the \texttt{[radiogroup]} object (with `Item Type' set to `Check Boxes' and `Output Using Single Number' set). Choose one of these methods so that every possible combination of button press is realised as unique number in the range 0 to 7:

\begin{center}
\includegraphics[width=0.7\textwidth]{images/binary}
\end{center}

Now modify the patch so that each time a button is pressed one of the seven corresponding notes is sent to the `AU DLS Synth 1'. You will need to use the \texttt{[noteout]} object for this - don't forget about note-off messages! Look up \texttt{[makenote]} if in doubt.

\subsubsection{Exercise - Feedback}
Add feedback such that when keys are pressed the single RGB LED switches between one of the seven different additive RGB colours (see image below (not so good if you're reading in black and white)) so that players are provided with some primary feedback (primary feedback) before notes are played using breath control (see next section). To do this you must use the analogue outputs of x-OSC, refer to practical 5 to remind yourself how to connect the RGB LED. Now you should see visual feedback to let you know that the buttons had been pressed.

\begin{center}
\includegraphics[width=0.3\textwidth]{images/rgbColours}
\end{center}

\subsubsection{Exercise - Breath Control}
When playing a trumpet, sound is not produced when the key is pressed, it is produced when air is blown into the mouthpiece. We do not have a breath controller so we will have to improvise. In your box you will find some white tack which can be used to mount the flex sensor on the front or side of the breadboard carrier such that you can bend it by blowing, you may need to attach a paper sail to the flex sensor (also with white tack) such that it bends sufficiently when blown. Now modify your patch so that the trumpet note is set by the key combination, but a MIDI note on message is not sent until the the flex sensor is blown, and the MIDI note off event is sent when the flex sensor returns to normal (you will need to use the hysteresis process to ensure stable note on/off transitions). You may want to refer to practical three to remind yourself how to connect the flex sensor. 

\subsubsection{Exercise - MIDI Routing to Logic}
The `AU DLS Synthesiser 1' is very limited so instead we will route the MIDI messages to Logic where we have a much more choice in terms of sound design. 
\begin{enumerate}
\item launch Logic Pro X. 
\item if you have not used Logic at UWE previously then select `Create a new empty project'. If an older project has loaded then select File $\rightarrow$ New Project and click close to shut the old project.
\item in the next window create a new `Software Instrument' track.
\item now, in the `Library' (left column) select `Orchestral' $\rightarrow$ `Brass' $\rightarrow$ `Trumpets' 

\begin{center}
\includegraphics[width=0.9\textwidth]{images/logictrump}
\end{center}

\item you should now be able to play the instrument on your Impulse. 

\item return to your Max patch and on the \texttt{[noteout]} object, select `from Max 1'. You should now be able to play the Logic sound using your interface.

\end{enumerate}

\subsubsection{Exercise - Adding Expression}
We will now add further control parameters to add additional expression.
\begin{enumerate}
\item bring up the interface to the instrument by double clicking the EXS24 in the instrument slot. 
\begin{center}
\includegraphics[width=0.9\textwidth]{images/logicAndExs}
\end{center}

\item on the interface turn on the `Filter parameters' by clicking the button to the right of the `Cutoff' control
\begin{center}
\includegraphics[width=0.7\textwidth]{images/exs}
\end{center}

\item make MIDI assignments to the `Drive' and the `Cutoff' parameters to the Impulse slider 1 (control number 41) and slider 2 (control number 42) faders respectively. To do this: 
\begin{itemize}
\item press cmd + L
\item with the mouse, click on the `Drive' parameter
\item move slider 1
\end{itemize}
repeat the steps above to assign the D10 fader to the `Cutoff' parameter. When complete press cmd + L again to prevent Logic from making further parameter mappings.
\end{enumerate}

Note that while a trumpet player uses their mouth and three fingers on one hand, they have lots of `spare bandwidth', they can freely move the instrument around. This makes the adding of expression using the  accelerometer to control expression with pitch and roll a meaningful addition to this interface, you may wish to recall how this device is used in practical 6. Add the accelerometer to the physical interface and use the pitch and roll readings to to control the `Drive' and `Cutoff' parameters in Logic. 

To send these messages you will need to send messages from MaxMSP to Logic using the \texttt{[ctlout]} you can send this object a list of two numbers where the first is the control value and the second is the control number. See for example in the patch shown below two virtual sliders send the same control messages as slider 1 and 2 on the Impulse:

\begin{center}
\includegraphics[width=0.2\textwidth]{images/ctlout}
\end{center}

Make sure that the slider is sending numbers in the range 0 - 127 (which it does by default), and the \texttt{[ctlout]} object is using the `from Max 1' MIDI port. To get logic to listen to to Max rather than the Impulse we have to turn on some of the `Advanced' features in Logic. 

\begin{enumerate}
\item in the `Logic Pro X' menu select `Preferences' $\rightarrow$ `Adcanced Tools...'
\item in the Preferences window, under the in the `Advanced Options' section check `Control Surfaces', and close the window.
\item again, under the `Logic Pro X' menu select `Control Surfaces' then `Controller Assignments' and click the `Expert View' tab.
\item you should see the two assignments for the Filter Cutoff and Drive parameters. 
\item click on one of the assignments and in the `MIDI Input Message' change the `Input' from `Impulse  Impulse' to `from Max 1'.
\item do the same for the other parameter assignment.
\item sometimes the settings in the `Value' section can be incorrect causing strange non-linear parameter control. The correct settings are shown in the image below should you run into difficulties.
\begin{center}
\includegraphics[width=0.7\textwidth]{images/assignments}
\end{center}
\end{enumerate}

\subsubsection{Exercise - Modify and Extend}
Feel free to add additional controls. For example you could add feedback for the pitch and roll, by the brightness of two LEDs; you could also add additional buttons so that it is possible to play a wider range of notes, perhaps looking to clarinet key mappings for inspiration.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finishing up}
Create a \texttt{\textbf{Practical07}} folder in your homespace and save everything for safe keeping. 



